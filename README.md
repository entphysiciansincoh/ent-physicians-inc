For over 35 years, ENT Physicians Inc. have provided medical and hearing health care to families in the Toledo community. Our experienced team consists of Physicians, Certified Nurse Practitioners, Audiologists and Allergy Specialists.

Address: 1050 Isaac St, Suite 137, Oregon, OH 43616

Phone: 419-698-4505
